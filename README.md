# Homedash

![alt text](https://i.imgur.com/10yT9sO.png "Homedash")
### A flask based dashboard for your home and family

## Setup

### Clone the repository and cd into the folder:

```git clone https://gitlab.com/LanceMaverick/homedash.git```

```cd homedash```

### Create a virtual environment for python 3, e.g:

```virtualenv venv -p /usr/bin/python3.6```

or equivalent (conda, pyenv etc...)

### pip install the requirements:

```pip install -r requirements.txt```

### Move the config example and fill it in:

```cp dash/config.py.example dash/config.py```

make sure to set the `SALT` and `FLASK_KEY` to something other than the defaults,
such as random strings. If you wish you can also fill in the `key` and location
values for the open weather map API. You can get a free API key at: 
https://openweathermap.org/api


### Create the database tables:

```python create_all.py```

### Create the migrations:

```python manage.py db init```

```python manage.py db migrate```

```python manage.py db upgrade```

### Create a user account:

```python manage_users.py create```

and at the prompts, type in your username and password.

### Run the dev server:

```python run.py```

And there you go! It will be available at http://localhost:5000

For running a flask app in production on, for example, a digital ocean VPS, see:
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-uswgi-and-nginx-on-ubuntu-18-04




