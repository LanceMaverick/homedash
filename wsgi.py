from dash.logconf import configure_logging
from dash import create_app

configure_logging()
app = create_app()

if __name__ == "__main__":
    app.run()
