import os
import json
import time
from datetime import datetime, timezone
from datetime import timedelta 
from .config import OwmConf
import pyowm
from pyowm.tiles.enums import MapLayerEnum
from pyowm.utils.geo import Point
from pyowm.commons.tile import Tile
#from flask import current_app as app

    

def get_weather(fpath, cache_age=3600):
    if os.path.isfile(fpath) and (time.time() - os.path.getmtime(fpath) < cache_age):
        try:
            with open(fpath, 'r') as outfile:
                weather_json = json.load(outfile)
                return weather_json
        except json.decoder.JSONDecodeError:
                return gen_weather(fpath)    
    else:
        return gen_weather(fpath)
    

def gen_weather(fpath):
    if not OwmConf.key:
        return None
    owm = pyowm.OWM(OwmConf.key)
    w = owm.weather_at_place(OwmConf.location).get_weather()
    fc = owm.daily_forecast(OwmConf.location, limit=6).get_forecast()
    #turn weather data into dict
    def to_dict(w):
        temp = w.get_temperature('celsius')
        if 'temp_min' in temp.keys():
            t0 = temp['temp']
            t1 = temp['temp_min']
            t2 = temp['temp_max']
        else:
            t0 = temp['day']
            t1 = temp['min']
            t2 = temp['max']
        return dict(
            wind = w.get_wind(),
            humidity = w.get_humidity(),
            t0 = t0,
            t1 = t1,
            t2 = t2,
            status = w.get_detailed_status(),
            icon = w.get_weather_icon_url()
        )
    w_dict = to_dict(w)
    w_dict['day'] = 'Today'
    fs = fc.get_weathers()
    fs_list = [to_dict(f) for f in fs]

    #add last update key
    data = dict(
        last_update = datetime.now(timezone.utc).strftime('%m/%d/%Y, %H:%M:%S'),
        weather = [w_dict])

    #add each forecast and the name of the day
    for i, f in enumerate(fs_list):
        i+=1
        date = datetime.now() + timedelta(days=i)
        day = date.strftime("%A")
        f['day'] = day
        data['weather'].append(f)
    
    #save data and return
    with open(fpath, 'w') as outfile:
        json.dump(data, outfile)

    return data

def get_forecast():
    owm = pyowm.OWM(OwmConf.key)
    fc = owm.daily_forecast(OwmConf.location, limit=6)
    return fc.get_forecast()

def get_maps():
    owm = pyowm.OWM(OwmConf.key)
    geopoint = Point(OwmConf.lon, OwmConf.lat)
    x_tile, y_tile = Tile.tile_coords_for_point(geopoint, 5)
    
    layer_name = MapLayerEnum.TEMPERATURE
    tm = owm.tile_manager(layer_name)
    tile = tm.get_tile(x_tile, y_tile, 5)
    tile.persist(os.path.join(app.instance_path, 'temp.png'))
    tm.map_layer = MapLayerEnum.PRECIPITATION
    tile = tm.get_tile(x_tile, y_tile, 5)
    tile.persist(os.path.join(app.instance_path, 'rain.png'))


def get_icon(status):
    index = dict(

    )


