import os
from flask import current_app as app
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory
)
import flask_mobility
from flask_security import login_required, current_user

from taskw import TaskWarrior

from dash.models import User
import dash.forms as dashforms

from .database import db


bp = Blueprint('task', __name__, url_prefix='/tasks')

prior_map = dict(
    H='High',
    M='Medium',
    L='Low'
)
def get_tasker(uid):
    path = os.path.join(app.instance_path, 'tasks', str(uid))
    taskrc = os.path.join(path, '.taskrc')
    task_path = os.path.join(path, '.task/')

    if os.path.isfile(taskrc):
        app.logger.info('retrieving task dir for user {}'.format(uid))
    else:
        os.makedirs(os.path.join(task_path, 'hooks/'))   
        with open(taskrc,'w') as f:
            f.write('data.location='+task_path)
        with open(task_path+'completed.data', 'w') as f:
            f.write('')
        with open(task_path+'pending.data', 'w') as f:
            f.write('')
        app.logger.info('new task dir setup for user id {}'.format(uid))
    return taskrc

def load_warrior(user):
    uid = user.id
    uconfig = get_tasker(uid)
    warrior = TaskWarrior(config_filename=uconfig, marshal=True)
    return warrior

def get_projects(w):
    projects = []
    tasks = w.load_tasks()

    for t in tasks['completed'] + tasks['pending']:
        p = t['project']
        if p and p not in projects:
            projects.append(p) 
    return projects

def get_task_diff(t1, t2):
    keys = [
        'description',
        'project',
        'due',
    ]
    diffs = dict()
    for k in keys:
        if t1[k] != t2[k]:
            diffs.update({k:t2[k]})
    return diffs


@bp.route('/')
@login_required
def index():
    warrior = load_warrior(current_user)
    tasks = warrior.load_tasks()
    
    return render_template('task/index.html', tasks=tasks, prior_map=prior_map)

@bp.route('/new', methods=['GET', 'POST'])
@login_required
def new_task():
    w = load_warrior(current_user)
    form = dashforms.TaskForm(request.form)
    form.project.choices = [('None', 'None/No Change')] + [(p, p) for p in get_projects(w)]
    if form.validate_on_submit():
        task = w.task_add(**form.get_task_dict())
        app.logger.info(task)
        flash('New task "{}" added', 'success'.format(form.description.data))
        return redirect(url_for('task.index'))
        
    return render_template('task/new.html', form=form)

@bp.route('/edit/<int:task_id>', methods=['GET', 'POST'])
def edit_task(task_id):
    w = load_warrior(current_user)
    id, task = w.get_task(id=task_id)
    form = dashforms.TaskForm()
    form.project.choices = [('None', 'None/No Change')] + [(p, p) for p in get_projects(w)]
    if form.validate_on_submit():
        app.logger.info(form.description.data)
        new_data = form.get_task_dict()
        #changes = get_task_diff(task, new_data)
        #app.logger.info('Changes:')
        #app.logger.info(changes)

        for k, v in new_data.items():
            task[k] = v
        app.logger.info(new_data)
        app.logger.info(task)
        w.task_update(task)
        return redirect(url_for('task.index'))
    
    form.populate_from_task(task)
    return render_template('task/new.html', form=form)

@bp.route('/delete/<int:task_id>', methods=['POST'])
def delete_task(task_id):
    w = load_warrior(current_user)
    id, task = w.get_task(id=task_id)
    w.task_delete(id=task_id)
    flash('Task "{}" deleted'.format(task['description']), 'danger')
    return redirect(url_for('task.index'))

@bp.route('/complete/<int:task_id>', methods=['POST'])
def complete_task(task_id):
    w = load_warrior(current_user)
    id, task = w.get_task(id=task_id)
    w.task_done(id=task_id)
    flash('Task "{}" completed!'.format(task['description']), 'success')
    return redirect(url_for('task.index'))






