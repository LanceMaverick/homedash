from datetime import datetime
from .database import db
#from werkzeug.security import check_password_hash, generate_password_hash
from flask_security.utils import hash_password, verify_and_update_password
from flask_security import UserMixin, RoleMixin, utils

roles_users = db.Table('roles_users',
        db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
        db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False, index=True)
    password = db.Column(db.String(), nullable=False)
    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    
    def check_password(self, password):
        return verify_and_update_password(password, self)
    
    def set_password(self, password):
        self.password = hash_password(password)

    def __repr__(self):
        return '<User %r>' % self.username
    

class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))

#items = db.Table('items',
#        db.Column('item_id', db.Integer(), db.ForeignKey('item.id')),
#        db.Column('shopping_id', db.Integer(), db.ForeignKey('shopping.id')))    

#TODO: move date to Shopping
class Item(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120), unique=True, nullable=False)
    used = db.Column(db.Integer(),  default=0)
    shopping_id = db.Column(db.Integer, db.ForeignKey('shopping.id'))

class Shopping(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    item = db.relationship("Item", uselist=False, backref="shopping")
    quantity = db.Column(db.Integer(),  default=0)

    
    
    #items = db.relationship('ItemList', secondary=items,
    #                        backref=db.backref('Shopping', lazy='dynamic'))

class Note(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    title = db.Column(db.String(120), unique=True, nullable=False)
    note = db.Column(db.Text(),  nullable=True)
    active = db.Column(db.Boolean(),  nullable=False, default=True)

class Temperature(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime(), nullable=False, default=datetime.utcnow)
    temp = db.Column(db.Float(), nullable=False)

    def __repr__(self):
        return '<Temperature %r>' % self.temp




