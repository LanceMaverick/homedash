import os
import sys
import json
import datetime

#from bokeh.embed import components
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory
)
from flask import current_app as app
import flask_mobility
from werkzeug.exceptions import abort
from werkzeug import secure_filename
from flask_security import login_required, current_user
from dash.models import User, Shopping, Item, Note, Temperature
import dash.forms as dashforms

from .database import db

from .weather import get_weather, get_maps, get_forecast

bp = Blueprint('dashboard', __name__)

#TODO: Better weather implementation
@bp.route('/')
@login_required
def index():
    weather = get_weather(os.path.join(app.instance_path, "weather.json"))
    
    internal_temp = db.session.query(Temperature).order_by(Temperature.id.desc()).first().temp
    #TODO: weather maps
    #get_maps()
    shopping_list = Shopping.query.all()
    notes = Note.query.all()
    return render_template(
        'dash/index.html',
        notes = notes,
        shopping = shopping_list,
        weather = weather,
        internal_temp = internal_temp)

### shopping
@bp.route('/shopping', methods=('GET', 'POST'))
@login_required
def shopping():
    
    shopping_list = Shopping.query.all()
    items = Item.query.all()
    
    new_item_form = dashforms.AddNewItem(request.form, prefix="new")
    choose_item_form = dashforms.ChooseItem(request.form, prefix="choose" )
    choose_item_form.item.choices= [(i.id, i.name) for i in items]

    if new_item_form.validate_on_submit():
        name = new_item_form.name.data.lower()
        quantity = new_item_form.quantity.data
        
        item_check = Item.query.filter_by(name=name).first()
        if item_check:
            new_item = item_check
            new_item.quantity+=1
        else:
            new_item = Item(name=name, used=1)
            db.session.add(new_item)
        
        new_shopping_item = Shopping(item=new_item, quantity=quantity)
        db.session.add(new_shopping_item)
        db.session.commit()
        flash('{}x {}" has been added to the list'.format(quantity, name), 'success')
        return redirect(url_for('dashboard.shopping'))
    
    if choose_item_form.validate_on_submit():
        new_item = Item.query.filter_by(id=choose_item_form.item.data).first()
        new_item.used+=1
        quantity = choose_item_form.quantity.data
        if Shopping.query.filter_by(item=new_item).first():
            flash('Cannot add {} as it is already in the shopping list!'.format(new_item.name), 'warning')
            return redirect(url_for('dashboard.shopping'))              
        else: 
            shopping_item = Shopping(item=new_item, quantity=quantity)
            db.session.add(shopping_item)
            db.session.commit()
            flash('{}x {}" has been added to the list'.format(quantity, new_item.name), 'success')
            return redirect(url_for('dashboard.shopping'))

    return render_template(
        'shopping/shopping.html',
        shopping=shopping_list,
        new_form=new_item_form,
        choose_form=choose_item_form)


@bp.route('/shopping/<int:item_id>/edit', methods=('GET', 'POST'))
@login_required
def edit_shopping(item_id):
    shopping_items = Shopping.query.all()
    item = Shopping.query.filter_by(id=item_id).first()
    form = dashforms.EditQuantity(request.form)
    #form.quantity.data=item.quantity
    if form.validate_on_submit():
        item.quantity = form.quantity.data
        db.session.commit()
        flash('{} quantity has been updated to {}'.format(item.item.name, item.quantity), 'info')
        return redirect(url_for('dashboard.shopping'))
    else:
        form.quantity.data=item.quantity
    return render_template('shopping/edit_shopping.html', form = form, name = item.item.name, shopping=shopping_items)

@bp.route('/shopping/<int:item_id>/delete', methods=['POST'])
@login_required
def delete_shopping(item_id):
    item = Shopping.query.filter_by(id=item_id).first()
    item.item.used-=1
    db.session.delete(item)
    db.session.commit()
    flash('{} has been deleted'.format(item.item.name), 'warning')
    return redirect(url_for('dashboard.shopping'))

@bp.route('/shopping/<int:item_id>/bought', methods=['POST'])
@login_required
def bought_shopping(item_id):
    item = Shopping.query.filter_by(id=item_id).first()
    db.session.delete(item)
    db.session.commit()
    flash('{} has been ticked off!'.format(item.item.name), 'success')
    return redirect(url_for('dashboard.shopping'))

@bp.route('/clear_shopping', methods=['POST'])
@login_required
def clear_shopping():
    Shopping.query.delete()
    db.session.commit()
    flash('Shopping list has been cleared', 'warning')
    return redirect(url_for('dashboard.shopping'))

### items
@bp.route('/items')
@login_required
def items():
    items = Item.query.all()
    return render_template('shopping/items.html', items = items)

@bp.route('/item/<int:item_id>/delete', methods=['POST'])
@login_required
def delete_item(item_id):
    item = Item.query.filter_by(id=item_id).first()
    if Shopping.query.filter_by(item=item).first():
        flash('{} cannot be deleted as it is in the current shopping list!'.format(item.name), 'danger')
    else:
        db.session.delete(item)
        db.session.commit()
        flash('{} has been deleted from the common items list'.format(item.name), 'warning')
    return redirect(url_for('dashboard.items'))

### notes
@bp.route('/notes')
@login_required
def notes():
    notes = Note.query.all()
    return render_template('notes/notes.html', notes=notes)

@bp.route('/notes/create_note', methods=['GET', 'POST'])
@login_required
def create_note():
    form = dashforms.NoteForm(request.form)
    if form.validate_on_submit():
        title = form.title.data
        note = form.note.data
        new_note = Note(title = title, note = note)
        db.session.add(new_note)
        db.session.commit()
        return redirect(url_for('dashboard.notes'))
    return render_template('notes/note.html', form=form)

@bp.route('/notes/<int:note_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_note(note_id):
    note = Note.query.filter_by(id=note_id).first()
    form = dashforms.NoteForm(request.form, obj=note)
    if form.validate_on_submit():
        form.populate_obj(note)
        db.session.commit()
        return redirect(url_for('dashboard.notes'))
    return render_template('notes/note.html', form=form, note_id = note.id, note_title = note.title)

@bp.route('/notes/<int:note_id>/delete', methods=['POST'])
@login_required
def delete_note(note_id):
    note = Note.query.filter_by(id=note_id).first()
    db.session.delete(note)
    db.session.commit()
    flash('{} has been deleted'.format(note.title), 'warning')
    return redirect(url_for('dashboard.notes'))



