import os
import sys
import json
from flask_cors import cross_origin
from datetime import datetime, timedelta
from flask import current_app as app
from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, send_from_directory, Response
)
import flask_mobility
from flask_security import login_required, current_user

from bokeh.plotting import figure, show
from bokeh.embed import components
from bokeh.models.widgets import DateRangeSlider
from bokeh.plotting import figure

import pandas as pd

from dash.models import User, Temperature
import dash.forms as dashforms
from .database import db
from .config import TempConf

bp = Blueprint('therm', __name__, url_prefix='/therm')

def temp_graph(temps, debug=False):

    if debug:
        import random
        first_date = datetime.now()
        delta = 0
        dates = []
        temps = []
        for i in range (0,18000):
            dates.append(first_date + timedelta(seconds=delta))
            temps.append(random.gauss(25, 7))
            delta += 1800
        data_dict = dict(
            date = dates,
            temp = temps
        )
    else:
        data_dict = dict(
            date = [t.date for t in temps],
            temp = [t.temp for t in temps]
        )
    df = pd.DataFrame.from_dict(data_dict).sort_values(['date'])

    p = figure(plot_width=800, plot_height=250, x_axis_type="datetime")
    p.title.text = 'Internal Temperature'

    p.line(df['date'], df['temp'], line_width=2, color="red", alpha=0.8)
    

    return components(p)
def date_choice(c):
    c_map = dict(
        d = 1,
        w = 7,
        m = 30,
        m3 = 90,
        m6 = 180,
        y = 365
    )
    return datetime.now() - timedelta(days=c_map[c])
@bp.route('/', methods = ['GET', 'POST'])
@login_required
def temp_stream():
    form = dashforms.TempRangeForm(request.form)
    if form.validate_on_submit():
        range_choice = form.data_range.data
    else:
        range_choice = 'd'
    
    t_date_min = date_choice(range_choice)
    temps = Temperature.query.filter(Temperature.date > t_date_min).all()
    temp_script, temp_div = temp_graph(temps, debug=False)
    print('+++++', range_choice, t_date_min, len(temps), file=sys.stdout)

    return render_template(
        'therm/graph.html',
        form = form,
        script_graph = temp_script,
        div_graph = temp_div,
    )

@bp.route('/webhook/{}'.format(TempConf.token), methods=['GET', 'POST'])
@cross_origin()
def temp_hook():
    if request.method == "GET":
        return "hello world"
    data = json.loads(request.data)
    date = datetime.now()
    temp = float(data['temp'])
    temperature = Temperature(temp=temp, date=date)
    print(temperature, file=sys.stdout)
    db.session.add(temperature)
    db.session.commit()
    return Response('Temp Added', 200)
