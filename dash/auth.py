import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash
from flask_security import login_user, logout_user, current_user, login_required
from flask_security.forms import ChangePasswordForm
from flask import current_app as app

from dash.models import User
from dash.forms import LoginForm
from .database import db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register')
def register():
    return "Registering is handled manually by the admin."

#login view
@bp.route('/login', methods=('GET', 'POST'))
def login():
    next_param = request.args.get('next')
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm(request.form)
    if form.validate_on_submit():
        username = form.username.data.lower()
        password = form.password.data
        error = None
        user = User.query.filter_by(username=username).first()
        if user is None or not user.check_password(password):
            flash('Incorrect username or password.','danger')
            return redirect(url_for('auth.login', form=form, next=next_param))
        login_user(user)
        if next_param:
                return redirect(next_param)
        #    session.clear()
        #    session['user_id'] = user.id
        return redirect(url_for('index', error=error))

    return render_template('auth/login.html', form=form)

@login_required
@bp.route('/account', methods=('GET', 'POST'))
def account():
    user = current_user
    form = ChangePasswordForm(request.form)
    if request.method =='POST':
        if form.validate():
            password = form.new_password.data
            user.set_password(password)
            db.session.commit()
            flash('Password updated!', 'success')
        else:
            flash('ERROR', 'danger')
            for e in form.password.errors:
                flash(e, 'danger')
    return render_template('auth/account.html', form=form)

@bp.before_app_request
def load_logged_in_user():
    g.user = current_user

@bp.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))