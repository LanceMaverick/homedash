import os
from flask import Flask
from flask_assets import Environment, Bundle
from flask_cors import CORS
from flask_bootstrap import Bootstrap
#from flask_sqlalchemy import SQLAlchemy
from flask_scss import Scss
#from flask_alembic import Alembic
from flask_migrate import Migrate
from flask_login import LoginManager
from .database import db
from .config import config

#test_config = ''
basedir = os.path.abspath(os.path.dirname(__file__))

def create_app(test_config=None):
# create and configure the app
    app = Flask(__name__, instance_relative_config=True, instance_path=os.path.join(basedir, 'static/instance/'))
    cors = CORS(app)
    app.config['CORS_HEADERS'] = 'Content-Type'


    assets = Environment(app)
    assets.url = app.static_url_path
    scss = Bundle(
        #'css/_variables.scss',
        'css/app.scss',
        'css/icons.scss',
        'css/login.scss',
        depends=('css/_variables.scss'),
        filters='pyscss',
        output='css/all.css')
    assets.register('scss_all', scss)
    app.config.from_mapping(
        SECRET_KEY=config.FLASK_KEY,
        UPLOAD_FOLDER = os.path.join(app.instance_path, 'images/uploads'),
        SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'app.db'),
        SQLALCHEMY_TRACK_MODIFICATIONS = False,
        SECURITY_PASSWORD_HASH = 'bcrypt',
        SECURITY_PASSWORD_SALT=config.SALT,
        SECURITY_USER_IDENTITY_ATTRIBUTES = 'username',
        SECURITY_LOGIN_URL = '/auth/login'
    )
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    Bootstrap(app)
#alembic.init_app(app)
    if test_config is None:
        # load the instance config, if it exists, when not testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # load the test config if passed in
        app.config.from_mapping(test_config)
      # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path, exist_ok=True)
        os.makedirs(os.path.join(app.instance_path, '/images/uploads'), exist_ok=True)

    except OSError:
        pass

#db init
#manager = Manager(app)
#manager.add_command('db', MigrateCommand)
       
#setup instance folder for uploads

#auth bp register
    from . import auth
    app.register_blueprint(auth.bp)

#awards bp register
 #   from . import admin
 #   app.register_blueprint(admin.bp)
      
#dashboard bp register
    from . import dashboard
    app.register_blueprint(dashboard.bp)
    app.add_url_rule('/', endpoint='index')

#therm bp register
    from . import therm
    app.register_blueprint(therm.bp)

#dashboard bp register
# removed until taskwarrior seen my systemd
#    from . import task
#    app.register_blueprint(task.bp)

#api register
#    from . import api
#    app.register_blueprint(api.bp, prefix='/api')
#    #app.add_url_rule('/api', endpoint = 'api')

#init db
    db.init_app(app)
    migrate = Migrate(app, db)
    from dash import models

#init login manager
    login_manager = LoginManager()
    login_manager.init_app(app)
    login_manager.login_view = 'auth.login'
    #test only TODO
    @login_manager.user_loader
    def load_user(user_id):
        return None

#set up flask security
    from flask_security import Security, SQLAlchemyUserDatastore
    user_datastore = SQLAlchemyUserDatastore(db, models.User, models.Role)
    security = Security()
    security.init_app(app, user_datastore)
    security.login_view = 'auth.login'
    
#global template functions
    def nl2br(s):
        return s.replace('\n', '<br>')

    app.jinja_env.globals.update(nl2br=nl2br)

    def nokey(obj, key):
        try:
            return obj[key]
        except KeyError:
            return None

    app.jinja_env.globals.update(nokey=nokey)

    return app

if __name__ == '__main__':
    

    #run main app
    app = create_app()
    app.run()
