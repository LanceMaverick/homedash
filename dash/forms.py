from flask_wtf import FlaskForm
#from flask_security.forms import LoginForm
from wtforms.ext.sqlalchemy.orm import model_form
from wtforms import SubmitField, StringField, PasswordField, SelectField, SubmitField, TextAreaField, BooleanField, HiddenField, IntegerField, validators
from wtforms.fields.html5 import DateField
from wtforms.validators import DataRequired

from flask_wtf.file import FileField, FileRequired, FileAllowed
from flask_uploads import UploadSet, IMAGES

from dash.models import User
from dash import db

class LoginForm(FlaskForm):
    username = StringField(u'Username', [validators.required(), validators.length(max=120)])
    password = PasswordField(u'Password', [validators.required()])
    submit = SubmitField('Sign In')

class AddNewItem(FlaskForm):
    name = StringField(u'Name', [validators.required(), validators.length(max=120)])
    quantity = IntegerField('Quantity', [validators.NumberRange(min=1, max=1000)])
    submit = SubmitField('Add')

class ChooseItem(FlaskForm):
    item = SelectField('Item', coerce=int)
    quantity = IntegerField('Quantity', [validators.NumberRange(min=1, max=1000)])
    submit = SubmitField('Add')

class EditQuantity(FlaskForm):
    quantity = IntegerField('Quantity', [validators.NumberRange(min=1, max=1000)])
    submit = SubmitField('Update')

class NoteForm(FlaskForm):
    title = StringField(u'Name', [validators.required(), validators.length(max=120)])
    note = TextAreaField(u'Note', [validators.optional(), validators.length(max=1000)])
    submit = SubmitField('Submit')


#task forms
class TaskForm(FlaskForm):
    description = StringField(u'Description',[validators.required(), validators.length(max=120)])
    new_project = StringField(u'Add to a new Project:')
    project = SelectField(u'Or choose an existing one:', coerce=str)
    due = DateField(u'Due date (optional)', format='%Y-%m-%d',validators=[validators.Optional()])
    priority = SelectField(u'Priority level:', choices=[('H', 'High'), ('M', 'Medium'), ('L', 'Low') ])
    submit = SubmitField(u'Submit')


    def get_project_choice(self):
        new = self.new_project.data
        existing = self.project.data
        if new:
            return new
        else:
            if existing == "None":
                return None
            else:
                return existing

    def get_task_dict(self):
        data = dict(
            description= self.description.data,
            priority=self.priority.data
        )

        project = self.get_project_choice()
        if project:
            data['project'] = project
        
        due = self.due.data
        if due:
            data['due'] = due
        
        return data

    def populate_from_task(self, task):
        self.description.data = task['description']
        if 'project' in task.keys():
            self.new_project.data = task['project']
        if 'due' in task.keys():
            self.due.data = task['due']
            #.strftime('%Y-%m-%d')
        self.priority.data = task['priority']


class TempRangeForm(FlaskForm):
    data_range = SelectField(u'Select data range:', choices=[('d', 'Day'), ('w', 'Week'), ('m', 'Month'), ('m3', '3 Months' ), ('m6', '6 Months'), ('y', 'Year')  ])
    submit = SubmitField('Update')

